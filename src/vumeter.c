#include <sys/mman.h>
#include <sys/time.h>
#include <fcntl.h>
#include <math.h>
#include <unistd.h>

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <vbx-libvumeter/vumeter.h>


#define RATE 48000
#define NUM_CHANNELS 2
#define LMAX_AGE 2.0 // 2s
#define CLIP_DEC_AGE 0.1 // 100ms
#define CLIP_AGE 10.0 // 10s
#define UPDATE_PERIOD 0.08 // vu-meter is updated every 80ms
#define DB_MIN -60.0
#define RMS_REF 32767.0 // this corresponds to a full scale sine signal at 1000Hz
#define CLIP_BOUNDARY 3

#define FB_WIDTH 1280
#define FB_HEIGHT 720

#define FB_STEP_HEIGHT      8
#define FB_BAND_HEIGHT      6
#define FB_MARGIN_HEIGHT    2
#define FB_LMAX_WIDTH       3
#define FB_CLIP_WIDTH       12

#define FB_POSX          372
#define FB_POSY          168
#define FB_DB0_WIDTH     864
#define FB_DB24_WIDTH    432

#define RGB24_TO_RGB565(x) \
  ((((x>>19) & 0x1f) << 11)  |  (((x >> 11) & 0x1f) << 6)  |  ((x >> 3) & 0x1f))

#define BLACK               RGB24_TO_RGB565(0x000000)
#define DARK_SLATE_GRAY     RGB24_TO_RGB565(0x2f4f4f)
#define MY_DARK_RED         RGB24_TO_RGB565(0x4f2f2f)
#define MY_HALF_RED         RGB24_TO_RGB565(0xb00000)
#define MY_FULL_RED         RGB24_TO_RGB565(0xff0000)
#define MEDIUM_PURPLE       RGB24_TO_RGB565(0x9370DB)
#define MEDIUM_SLATE_BLUE   RGB24_TO_RGB565(0x7B68EE)
#define VIOLET              RGB24_TO_RGB565(0xEE82EE)
#define SLATE_BLUE          RGB24_TO_RGB565(0x6A5ACD)
#define WHITE               RGB24_TO_RGB565(0xffffff)
#define GRAY                RGB24_TO_RGB565(0xcccccc)
#define ORANGE              RGB24_TO_RGB565(0xc9742e)
#define AQUAMARINE          RGB24_TO_RGB565(0x56BD9A)

static const uint16_t LEVEL_COLORS[] = {MEDIUM_PURPLE, MEDIUM_SLATE_BLUE};
static const uint16_t LMAX_COLORS[] = {VIOLET, VIOLET};
static const uint16_t CLIP_COLORS[] = {MY_DARK_RED, MY_HALF_RED, MY_FULL_RED};
static const uint16_t STATE_COLORS[] = {
  DARK_SLATE_GRAY, // VUMETER_STATE_ACTIVE
  AQUAMARINE,      // VUMETER_STATE_PAUSE
  MY_HALF_RED      // VUMETER_STATE_ERROR
};

#define MAX_SUMS 50

typedef struct chan_s
{
  struct
  {
    double db;
    double time;
  } level;
  struct
  {
    double db;
    double time;
  } lmax;
  struct
  {
    int8_t count;
    int8_t value;
    double time;
  } clip;

  int    rms_sums_len;
  double rms_sums[MAX_SUMS];
} chan_t;

static chan_t *channels[NUM_CHANNELS] = {NULL, NULL};
static int64_t rms_size = 0;
static double rms_time_ = 0.0;
static double pause_delay_ = -1.0;
static double pause_time_ = -1.0;
static vumeter_state_t state_ = VUMETER_STATE_ACTIVE;

typedef union fbshape_u
{
  uint16_t _2d[FB_HEIGHT][FB_WIDTH];
} fbshape_t;

static fbshape_t * fbmem_;
static int fbfd_ = -1;
static bool initialized_ = false;
static vumeter_layout_t layout_;

//#define FAKE_BUFFER

#ifdef FAKE_BUFFER
static const int sin1000_fs_size = 44;

static int16_t sin1000_fs_data[44] = {
  0, 4663, 9231, 13611, 17715, 21457, 24763, 27565,
  29805, 31439, 32433, 32767, 32433, 31439, 29805, 27565,
  24763, 21457, 17715, 13611, 9231, 4663, 0, -4663,
  -9231, -13611, -17715, -21457, -24763, -27565, -29805, -31439,
  -32433, -32767, -32433, -31439, -29805, -27565, -24763, -21457,
  -17715, -13611, -9231, -4663
};

static const int sin1000_hs_size = 44;

static int16_t sin1000_hs_data[44] = {
  0,  2331,  4615,  6805,  8857, 10728, 12381, 13782,
  14902, 15719, 16216, 16383, 16216, 15719, 14902, 13782,
  12381, 10728, 8857, 6805, 4615, 2331, 0, -2332, -4616,
  -6806, -8858, -10729, -12382, -13783, -14903, -15720,
  -16217, -16384, -16217, -15720, -14903, -13783, -12382,
  -10729, -8858, -6806, -4616, -2332
};

static int16_t square_fs_data[4] = { 32767, 32767, -32767, -32767 };
static int16_t square_fs_size = 4;

/* #define fake_buffer_data sin1000_fs_data */
/* #define fake_buffer_size sin1000_fs_size */
/* --> -3 dBFS */

/* #define fake_buffer_data sin1000_hs_data */
/* #define fake_buffer_size sin1000_hs_size */
/* --> -9 dbFS */

/* #define fake_buffer_data square_fs_data */
/* #define fake_buffer_size square_fs_size */
/* --> +0 dBFS */

#endif


static void paint_meter(void);
static void paint_box(int posx, int posy, int width, int height, uint16_t pixel);
static void reset_channel(chan_t *chan);
static void update_lmax(chan_t *chan);
static void update_clip_up(chan_t *chan, int value);
static void update_clip_down(chan_t *chan);
static chan_t *create_channel();
static void compute_and_store_sum_of_squares(chan_t * chan, int16_t *buffer, int size, int mod, int step);
static double compute_rms(chan_t * chan);
static int convert_db(double db);
static double get_time();

int vumeter_init(void)
{
  int i;

  if (initialized_) {
    fprintf(stderr, "libvumeter: already initialized\n");
    return -1;
  }

  for(i=0 ; i<NUM_CHANNELS ; i++) {
    channels[i] = create_channel();
  }
  rms_time_ = 0.0;

  fbfd_ = open ("/dev/fb0", O_RDWR);
  if (fbfd_ == -1) {
    fprintf(stderr, "libvumeter: cannot open framebuffer\n");
    return -1;
  }

  fbmem_ = (fbshape_t *)mmap (0, FB_WIDTH*FB_HEIGHT*2,
      PROT_WRITE, MAP_SHARED, fbfd_, 0);
  if (fbmem_ == MAP_FAILED) {
    close(fbfd_);
    fprintf(stderr, "libvumeter: cannot map framebuffer\n");
    return -1;
  }

  layout_.posx = FB_POSX;
  layout_.posy = FB_POSY;
  layout_.width = FB_DB0_WIDTH + FB_CLIP_WIDTH;
  layout_.height = FB_BAND_HEIGHT*2 + FB_STEP_HEIGHT + FB_MARGIN_HEIGHT*2;
  layout_.db_legend_min = -42.0;
  layout_.db_legend_max = 0.0;
  layout_.db_legend_step = 6.0;
  layout_.db_legend_format = "     %+4d";
  layout_.db_legend_unit = "dBFS";
  layout_.scale_0db = FB_DB0_WIDTH;
  layout_.scale_m24db = FB_DB24_WIDTH;

  initialized_ = true;
  return 0;
}


vumeter_layout_t * vumeter_get_layout(void)
{
  return &layout_;
}

int vumeter_feed (void *data, int size_p)
{
  int i;
  double rms, db, t;
  int size;

  size = size_p / 4;

  if (!initialized_) {
    fprintf(stderr, "libvumeter: cannot open framebuffer\n");
    return -1;
  }

  if (state_ == VUMETER_STATE_PAUSE) {
    if (pause_time_ > 0) {
      t = get_time() - pause_time_;
      if (t < pause_delay_)
        return 0;
    }
  }

  rms_time_ += ((double)(size)) / RATE;
#ifndef FAKE_BUFFER
  for(i=0; i<NUM_CHANNELS; i++)
    compute_and_store_sum_of_squares (channels[i], (int16_t *)data + i, size, size, 2);
#else
  compute_and_store_sum_of_squares (channels[0], fake_buffer_data, size, fake_buffer_size, 1);
  compute_and_store_sum_of_squares (channels[1], fake_buffer_data, size, fake_buffer_size, 1);
#endif
  rms_size += size;

  t = ((double)rms_size) / RATE;
  if (t < UPDATE_PERIOD)
    return 0;

  for(i=0; i<NUM_CHANNELS; i++) {
    rms = compute_rms(channels[i]);
    if (rms == 0.0)
      db = DB_MIN;
    else {
      db = -20 * log10(RMS_REF / rms);
    }
    channels[i]->level.db = db;

    update_clip_down (channels[i]);
    update_lmax(channels[i]);
  }
  rms_size = 0;

  state_ = VUMETER_STATE_ACTIVE;
  paint_meter();
  return 0;
}

static double get_time()
{
  struct timeval tv;
  double d;

  gettimeofday(&tv, NULL);
  d = tv.tv_sec + (tv.tv_usec / 1000000.0);
  return d;
}


int vumeter_set_pause_delay (double delay)
{
  if (!initialized_) {
    fprintf(stderr, "libvumeter: cannot open framebuffer\n");
    return -1;
  }

  pause_delay_ = delay;
  return 0;
}

int vumeter_set_state (vumeter_state_t state)
{
  int i;

  if (!initialized_) {
    fprintf(stderr, "libvumeter: cannot open framebuffer\n");
    return -1;
  }

  if ((state < 0) || (state >= VUMETER_STATE_MAX))
    return -1;

  for(i=0; i<NUM_CHANNELS; i++) {
    reset_channel(channels[i]);
  }
  rms_size = 0;
  state_ = state;

  if (state == VUMETER_STATE_PAUSE) {
    pause_time_ = get_time();
  }

  paint_meter();
  return 0;
}


static void update_lmax(chan_t *chan)
{
  double age;

  age = rms_time_ - chan->lmax.time;
  if ((chan->level.db > chan->lmax.db) || (age > LMAX_AGE)) {
    chan->lmax.db = chan->level.db;
    chan->lmax.time = rms_time_;
  }
}


static void update_clip_up(chan_t *chan, int value)
{
  if (value >= 32700 || value <= -32700) {
    chan->clip.count ++;
    if (chan->clip.count > CLIP_BOUNDARY) {
      chan->clip.count = CLIP_BOUNDARY;
      chan->clip.value = 2;
      chan->clip.time = rms_time_;
    }
  }
  else {
    chan->clip.count --;
    if (chan->clip.count < 0)
      chan->clip.count = 0;
  }
}


static void update_clip_down(chan_t *chan)
{
  double age;

  age = rms_time_ - chan->clip.time;
  if (chan->clip.value == 0)
    return;

  if (age > CLIP_AGE)
    chan->clip.value = 0;
  else if (age > CLIP_DEC_AGE)
    chan->clip.value = 1;
}


static chan_t *create_channel()
{
  chan_t *chan = malloc(sizeof(chan_t));
  reset_channel (chan);
  rms_size = 0;
  return chan;
}


static void reset_channel(chan_t *chan)
{
  chan->level.db = DB_MIN;
  chan->level.time = 0.0;
  chan->lmax.db = DB_MIN;
  chan->lmax.time = 0.0;
  chan->clip.count = 0;
  chan->clip.value = 0;
  chan->clip.time = 0.0;
  chan->rms_sums_len = 0;
}


static void compute_and_store_sum_of_squares(chan_t * chan, int16_t *buffer, int size, int mod, int step)
{
  int i, j;
  double tmp;
  double sum;

  sum = 0.0;
  j = 0;
  for(i=0 ; i<size ; i++) {
    tmp = buffer[j];
    j = (j + step) % mod;
    update_clip_up (chan, tmp);
    sum += tmp * tmp;
  }

  if (chan->rms_sums_len < MAX_SUMS) {
    chan->rms_sums[chan->rms_sums_len] = sum;
    chan->rms_sums_len ++;
  }
  else
    fprintf(stderr, "libvumeter: MAX_SUMS (==%d) must be increased\n", MAX_SUMS);
}


static double compute_rms(chan_t * chan)
{
  double accum;
  int i;

  accum = 0.0;
  for (i = 0; i < chan->rms_sums_len; i++) {
    accum += chan->rms_sums[i];
  }
  accum = accum/rms_size;
  chan->rms_sums_len = 0;
  return sqrt(accum);
}


static int convert_db(double db)
{
  double alpha;
  int w;

  alpha = (FB_DB24_WIDTH - FB_DB0_WIDTH) / 24.0;
  w = FB_DB0_WIDTH  -  db * alpha;
  if (w < 0)
    w = 0;
  else if (w > FB_DB0_WIDTH)
    w = FB_DB0_WIDTH;
  return w;
}


static void paint_box (int posx, int posy, int width, int height, uint16_t pixel)
{
  int i, j;
  fbshape_t * m = (fbshape_t *)fbmem_;

  for (i=0; i<height; i++) {
    for (j=0; j<width; j++) {
      m->_2d[posy+i][posx+j] = pixel;
    }
  }
}


static void paint_meter(void)
{
  int i;
  double lvl, lmax;
  int clip, w, posy;

  posy = FB_POSY + FB_MARGIN_HEIGHT;

  for(i=0; i < NUM_CHANNELS; i++) {
    paint_box(FB_POSX, posy, FB_DB0_WIDTH, FB_BAND_HEIGHT, STATE_COLORS[state_]);

    clip = channels[i]->clip.value;
    paint_box(FB_POSX + FB_DB0_WIDTH, posy, FB_CLIP_WIDTH, FB_BAND_HEIGHT, CLIP_COLORS[clip]);

    lvl = channels[i]->level.db;
    w = convert_db(lvl);
    paint_box(FB_POSX, posy, w, FB_BAND_HEIGHT, LEVEL_COLORS[i]);

    lmax = channels[i]->lmax.db;
    w = convert_db(lmax);
    if (w >= FB_LMAX_WIDTH)
      paint_box(FB_POSX + w - FB_LMAX_WIDTH, posy, FB_LMAX_WIDTH, FB_BAND_HEIGHT, LMAX_COLORS[i]);

    posy += FB_STEP_HEIGHT;
  }
}
