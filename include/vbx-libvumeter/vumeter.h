#ifndef __VBX_VUMETER_INCLUDED__
#define __VBX_VUMETER_INCLUDED__

typedef enum vumeter_state_enum
{
  VUMETER_STATE_ACTIVE      = 0,
  VUMETER_STATE_PAUSE       = 1,
  VUMETER_STATE_ERROR       = 2,
  VUMETER_STATE_MAX         = 3,
} vumeter_state_t;

typedef struct vumeter_layout_struct
{
  int posx;
  int posy;
  int width;
  int height;
  double db_legend_min;
  double db_legend_max;
  double db_legend_step;
  const char * db_legend_format;
  const char * db_legend_unit;
  int scale_0db;
  int scale_m24db;
} vumeter_layout_t;

int vumeter_init (void);
int vumeter_feed (void * data, int size);
int vumeter_set_state (vumeter_state_t state);
int vumeter_set_pause_delay (double delay);
vumeter_layout_t * vumeter_get_layout(void);

#endif
